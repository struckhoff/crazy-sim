#include <vector>

class Cell
{
public:
    static const int32_t VELOCITY_COEF = 100;
    uint32_t mass;
    int32_t x, y;
    uint32_t radius;

    Cell();
    Cell(uint32_t n_mass, int32_t n_x, int32_t n_y);
    int32_t max_velocity();

};

class Galaxy
{
public:
    Galaxy();
    update();
    print();

private:
    std::vector<Cell> cells;
};