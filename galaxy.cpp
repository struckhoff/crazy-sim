#include <iostream>
#include <vector>
#include "galaxy.h"

Cell::Cell() : mass(1), x(0), y(0) {}
Cell::Cell(uint32_t n_mass, int32_t n_x, int32_t n_y) :
        mass(n_mass), x(n_x), y(n_y) {}

int32_t Cell::max_velocity() {
    return VELOCITY_COEF/mass + 10;
}

Galaxy::print() {
    std::vector<std::vector<char>> v;
    v.resize(80);
    for (auto& vect : v) {
        vect.resize(80);
    }
    for (auto& cell : cells) {
        v[cell.x][cell.y]++;
    }

    for (auto& row : v) {
        for (auto& point : row) {
            std::cout << point << " ";
        }
        std::cout << std::endl;
    }
}